#!/bin/bash

# Failover script for Percona MySQL Server 5.7

# Master and Slave server details
MASTER_IP="10.42.1.10"
SLAVE_IP="10.42.2.20"


if ! ping -c 1 $MASTER_IP >/dev/null 2>&1; then
    echo "Master server ($MASTER_IP) is down. Initiating failover."

    
    ssh user@$SLAVE_IP "mysql -e 'STOP SLAVE;'"

    
    ssh user@$SLAVE_IP "sed -i 's/server-id=2/server-id=1/' /etc/my.cnf"

    
    ssh user@$SLAVE_IP "systemctl restart mysql"

   
    ssh user@$MASTER_IP "sed -i 's/server-id=1/server-id=2/' /etc/my.cnf"
    ssh user@$MASTER_IP "systemctl restart mysql"

    echo "Failover completed successfully. Slave server is now the new master."
else
    echo "Master server ($MASTER_IP) is still running. No failover required."
fi
