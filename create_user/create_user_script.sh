#!/bin/bash

# MySQL database credentials
DB_HOST="localhost"
DB_USER="root"
DB_PASSWORD="" # write real time root password here
LOG_FILE="create_user_script.log"
BACKUP_DIR="/var/backups/mysql"
FAILOVER_HOST="backup_hostname" # write hostname of mysql server

# Function to log messages
log() {
    local message=$1
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $message" >> $LOG_FILE
}

# The username and password here
USERNAME="abo"
PASSWORD="\$ecurePassw0rd"

# Function to create a database user
create_user() {
    local username="$USERNAME"
    local password="$PASSWORD"

    # MySQL command to create the user
    mysql -h $DB_HOST -u $DB_USER -p$DB_PASSWORD -e "CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';"

    if [ $? -eq 0 ]; then
        log "User '$username' created successfully!"
    else
        log "Failed to create user '$username'."
    fi
}

# Function to backup the database
backup_database() {
    local backup_file="$BACKUP_DIR/database_$(date '+%Y%m%d%H%M%S').sql"

    # MySQL command to backup the database
    mysqldump -h $DB_HOST -u $DB_USER -p$DB_PASSWORD --all-databases > $backup_file

    if [ $? -eq 0 ]; then
        log "Database backup created: $backup_file"
    else
        log "Failed to create database backup."
    fi
}

# Function to restore the database from a backup
restore_database() {
    local backup_file="$1"

    # MySQL command to restore the database from backup
    mysql -h $DB_HOST -u $DB_USER -p$DB_PASSWORD < $backup_file

    if [ $? -eq 0 ]; then
        log "Database restored from backup: $backup_file"
    else
        log "Failed to restore database from backup: $backup_file"
    fi
}

# Function to set up failover
setup_failover() {
    local failover_cmd="ssh $FAILOVER_HOST /var/failover/failover_setup.sh"

    # Execute failover command
    eval "$failover_cmd"

    if [ $? -eq 0 ]; then
        log "Failover setup completed."
    else
        log "Failed to set up failover."
    fi
}

# Function to generate script documentation
generate_documentation() {
    echo "********************************************" >> $LOG_FILE
    echo "Script Documentation" >> $LOG_FILE
    echo "********************************************" >> $LOG_FILE
    echo "This script creates a database user in MySQL, logs messages to a log file, performs backups of the database, restores the database from a backup, and sets up failover." >> $LOG_FILE
    echo "To create a user, use the 'create_user' function and provide the username and password." >> $LOG_FILE
    echo "To backup the database, use the 'backup_database' function." >> $LOG_FILE
    echo "To restore the database from a backup, use the 'restore_database' function and provide the path to the backup file as an argument." >> $LOG_FILE
    echo "To set up failover, use the 'setup_failover' function." >> $LOG_FILE
    echo "********************************************" >> $LOG_FILE
}

# Main script

log "Starting user creation process..."

create_user "$USERNAME" "$PASSWORD"
